#ifndef ALARMTIME_H
#define ALARMTIME_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>

namespace Ui {
class alarmTime;
}

class alarmTime : public QDialog
{
    Q_OBJECT

public:
    explicit alarmTime(QString timestr, QString mp3File, QWidget *parent = nullptr);
    ~alarmTime();
    QString timetext="";
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

signals:
    void dateTimeChanged(const QDateTime &dateTime); // 自定义信号
    void mp3path(const QString path); // 自定义信号
    void cronStat(bool stats); //计划每天执行还是单次执行true:每天执行
    void clearCron();//清空闹钟


private:
    Ui::alarmTime *ui;
};

#endif // ALARMTIME_H
