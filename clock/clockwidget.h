#include <QWidget>
#include <QPainter>
#include <QTimer>
#include <QTime>
#include <QMouseEvent>
#include <QPainterPath>
#include <QMenu>
#include <QApplication>
#include <QInputDialog>
#include <QColorDialog>
#include <QSettings>
#include <QFileInfo>
#include <QSystemTrayIcon>
#include <QMediaPlayer>
#include <QAudioOutput>
#include <alarmtime.h>
#include <QDateTime>

class ClockWidget : public QWidget {
    Q_OBJECT

public:
    ClockWidget(QWidget *parent = nullptr);

    QTimer *timer;
    QTime currentTime = QTime::currentTime();
    QString title = "时钟大师";
    //半径
    int r = 300;
    // 字体大小
    int fontSize = 20;
    //窗体样式
    QColor fontColor = QColor("#fff");
    //边框样式
    QColor fontBorderColor = fontColor;

    QString version = "时钟大师 1.7";


    //设置时钟大小
    void initSize();

    //config
    QSettings  *settings;
    QString configFile = "clock.ini";
    QString configGroup = "default";
    //闹钟
    int armYear;
    int armMonth;
    int armDate;
    int hourAlarm;
    int minuAlarm;
    QString hourstr;
    QString minustr;
    bool armOpen = true;
    bool armEnable = false; //启用闹钟
    bool armStatus = true; //闹钟指针闪烁
    bool aramDataStatus = false;//是每天还是单次，每天是true,单次是false
    QMediaPlayer *player;
    QAudioOutput *audioOutput;
    //mp3
    QString mp3path = "clock.mp3";
    //闹钟设置窗体
    alarmTime *alarm;


    void writini(QString qkeys,QString qvalues,QString gname);
    QString readini(QString qkeys,QString gname);
    void writboolini(QString qkeys,bool qvalues,QString gname);
    bool readboolini(QString qkeys,QString gname);

    //设置托盘图标
    QSystemTrayIcon *trayIcon = new QSystemTrayIcon(this);
    void setTrayIcon();
    bool topSatus = false;
protected:
    void paintEvent(QPaintEvent *event) override;
    void drawHand(QPainter *painter, int centerX, int centerY, int value, int time, int length,int handWidth);

private slots:

    void updatetime();
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason reason);

    void mousePressEvent(QMouseEvent *event) override {
        if (event->button() == Qt::LeftButton) {
            isDragging = true;
            startMousePos = event->pos();
        }
        if (event->button() == Qt::RightButton) {
            // 右键按下时显示菜单
            showContextMenu(event->globalPos());
        }
    }

    void mouseMoveEvent(QMouseEvent *event) override {
        if (isDragging) {
            endMousePos = event->pos();
            QPoint diff = endMousePos - startMousePos;
            px = x() + diff.x();
            py = y() + diff.y();
            move(px,py);
            writini("px",QString::number(px),configGroup);
            writini("py",QString::number(py),configGroup);
        }
    }

    void mouseReleaseEvent(QMouseEvent *event) override {
        if (event->button() == Qt::LeftButton) {
            isDragging = false;
        }
    }

    void showContextMenu(const QPoint &pos);

    void onExit();
    //设置时钟大小
    void clockSize();
    //设置颜色
    void clockColor();
    //设置边框颜色
    void clockBorderColor();
    //取消置顶
    void offtop();
    //置顶
    void ontop();
    //设置闹钟
    void setalarm();
    //停止闹钟
    void stopalarm();
    //清空闹钟
    void clearCron();
    //保存闹钟数据
    void savealarm(const QDateTime dateTime);
    //保存铃声路径
    void saveMp3Path(const QString path);
    //计划每天执行还是单次执行true:每天执行
    void cronStat(bool stats);
private:
    bool isDragging;
    QPoint startMousePos;
    QPoint endMousePos;
    int px = 0;
    int py = 0;
};
