#include "alarmtime.h"
#include "ui_alarmtime.h"

alarmTime::alarmTime(QString timestr, QString mp3File, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::alarmTime)
{
    ui->setupUi(this);
    this->setWindowTitle("设置闹钟");
    ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
    ui->label_4->setText(timestr);
    ui->lineEdit->setText(mp3File);
    timetext = timestr;
}

alarmTime::~alarmTime()
{
    delete ui;
}

//计划是单次执行
void alarmTime::on_pushButton_clicked()
{
    emit cronStat(false);
    QDateTime datetime = ui->dateTimeEdit->dateTime();
    emit dateTimeChanged(datetime);
    timetext = datetime.toString("yyyy-MM-dd hh:mm");
    ui->label_4->setText(timetext);
}

//设置mp3文件路径
void alarmTime::on_pushButton_2_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select MP3 File", "", "MP3 Files (*.mp3)");
    if (!fileName.isEmpty()) {
        ui->lineEdit->setText(fileName);
        emit mp3path(fileName);
    }
}

//计划每天执行
void alarmTime::on_pushButton_3_clicked()
{
    emit cronStat(true);
    QDateTime datetime = ui->dateTimeEdit->dateTime();
    emit dateTimeChanged(datetime);
    if(timetext.isEmpty()){
        timetext = datetime.toString("hh:mm");
    }else{
        timetext += ";"+datetime.toString("hh:mm");
    }

    ui->label_4->setText(timetext);
}


void alarmTime::on_pushButton_4_clicked()
{
    emit clearCron();
    timetext = "";
    ui->label_4->setText(timetext);
}

